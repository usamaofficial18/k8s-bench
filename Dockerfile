FROM node:latest
# Copy and build app
COPY . /home/craft/k8s-bench
WORKDIR /home/craft/
RUN cd k8s-bench \
    && yarn \
    && yarn build \
    && yarn --production=true

FROM node:slim
# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh

# Add non root user and set home directory
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/k8s-bench
COPY --from=0 /home/craft/k8s-bench .
RUN chown -R craft:craft /home/craft

USER craft

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
