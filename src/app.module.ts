import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KubectlModule } from './kubectl/kubectl.module';
import { configOptions } from './constants/config-options';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [ConfigModule.forRoot(configOptions), AuthModule, KubectlModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
