export interface IPolicy {
  validate(...args): unknown;
}
