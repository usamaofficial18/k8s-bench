import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { BenchGuard } from './bench.guard';

describe('BenchGuard', () => {
  let guard: BenchGuard;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BenchGuard, { provide: ConfigService, useValue: {} }],
    }).compile();

    guard = module.get<BenchGuard>(BenchGuard);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });
});
