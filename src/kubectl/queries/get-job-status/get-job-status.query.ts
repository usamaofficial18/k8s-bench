import { IQuery } from '@nestjs/cqrs';

export class GetJobStatusQuery implements IQuery {
  constructor(public readonly jobName: string) {}
}
