import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import * as k8s from '@kubernetes/client-node';
import { ConfigService } from '@nestjs/config';
import { HttpException } from '@nestjs/common';
import { KubectlService } from '../../aggregates/kubectl/kubectl.service';
import { GetJobStatusQuery } from './get-job-status.query';
import { ERPNEXT_NAMESPACE } from '../../../constants/config-options';

@QueryHandler(GetJobStatusQuery)
export class GetJobStatusHandler implements IQueryHandler<GetJobStatusQuery> {
  constructor(
    private readonly kubectl: KubectlService,
    private readonly config: ConfigService,
  ) {}

  async execute(query: GetJobStatusQuery) {
    const batchV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.BatchV1Api);
    const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);

    try {
      const jobResponse = await batchV1Api.readNamespacedJobStatus(
        query.jobName,
        namespace,
        'true',
      );
      delete jobResponse?.body?.metadata?.managedFields;
      return jobResponse?.body;
    } catch (error) {
      throw new HttpException(error, error?.statusCode || 500);
    }
  }
}
