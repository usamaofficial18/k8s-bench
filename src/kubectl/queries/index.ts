import { GetJobStatusHandler } from './get-job-status/get-job-status.handler';

export const KubeQueryHandlers = [GetJobStatusHandler];
