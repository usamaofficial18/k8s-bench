import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { DeleteSiteAggregateService } from '../../aggregates/delete-site-aggregate/delete-site-aggregate.service';
import { DeleteSiteCommand } from './delete-site.command';

@CommandHandler(DeleteSiteCommand)
export class DeleteSiteHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: DeleteSiteAggregateService,
  ) {}

  async execute(command: DeleteSiteCommand) {
    const { payload } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.deleteSiteJob(payload);
    aggregate.commit();

    return response;
  }
}
