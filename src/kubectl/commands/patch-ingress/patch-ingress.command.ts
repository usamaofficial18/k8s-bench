import { ICommand } from '@nestjs/cqrs';
import { PatchIngressDto } from '../../controllers/dtos/patch-ingress.dto';

export class PatchIngressCommand implements ICommand {
  constructor(public readonly payload: PatchIngressDto) {}
}
