import { InternalServerErrorException, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UpgradeSiteAggregateService } from '../../aggregates/upgrade-site-aggregate/upgrade-site-aggregate.service';
import { UpgradeSiteCommand } from './upgrade-site.command';

@CommandHandler(UpgradeSiteCommand)
export class UpgradeSiteHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: UpgradeSiteAggregateService,
  ) {}

  async execute(command: UpgradeSiteCommand) {
    const { payload } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    try {
      const res = await aggregate.upgradeSite(payload);
      aggregate.commit();
      return res;
    } catch (error) {
      Logger.error(error.toString(), error.toString(), this.constructor.name);
      throw new InternalServerErrorException(error);
    }
  }
}
