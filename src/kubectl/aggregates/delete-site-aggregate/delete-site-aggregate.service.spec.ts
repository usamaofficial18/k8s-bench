import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { KubectlService } from '../kubectl/kubectl.service';
import { DeleteSiteAggregateService } from './delete-site-aggregate.service';

describe('DeleteSiteAggregateService', () => {
  let service: DeleteSiteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeleteSiteAggregateService,
        { provide: ConfigService, useValue: {} },
        { provide: KubectlService, useValue: {} },
      ],
    }).compile();

    service = module.get<DeleteSiteAggregateService>(
      DeleteSiteAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
