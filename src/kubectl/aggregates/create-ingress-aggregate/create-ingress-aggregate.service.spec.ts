import { Test, TestingModule } from '@nestjs/testing';
import { CreateIngressAggregateService } from './create-ingress-aggregate.service';
import { INGRESS_CONFIG } from '../../providers/ingress-config.provider';
import { KubectlService } from '../kubectl/kubectl.service';

describe('CreateIngressAggregateService', () => {
  let service: CreateIngressAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CreateIngressAggregateService,
        { provide: INGRESS_CONFIG, useValue: {} },
        { provide: KubectlService, useValue: {} },
      ],
    }).compile();

    service = module.get<CreateIngressAggregateService>(
      CreateIngressAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
