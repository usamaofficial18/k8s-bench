import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { CreateSiteAggregateService } from './create-site-aggregate.service';
import { KubectlService } from '../kubectl/kubectl.service';

describe('CreateSiteAggregateService', () => {
  let service: CreateSiteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CreateSiteAggregateService,
        { provide: ConfigService, useValue: {} },
        { provide: KubectlService, useValue: {} },
      ],
    }).compile();

    service = module.get<CreateSiteAggregateService>(
      CreateSiteAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
