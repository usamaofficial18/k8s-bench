import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { KubectlService } from '../kubectl/kubectl.service';
import { UpgradeSiteAggregateService } from './upgrade-site-aggregate.service';

describe('UpgradeSiteAggregateService', () => {
  let service: UpgradeSiteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UpgradeSiteAggregateService,
        { provide: KubectlService, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<UpgradeSiteAggregateService>(
      UpgradeSiteAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
