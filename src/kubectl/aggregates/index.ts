import { CreateIngressAggregateService } from './create-ingress-aggregate/create-ingress-aggregate.service';
import { CreateSiteAggregateService } from './create-site-aggregate/create-site-aggregate.service';
import { DeleteIngressAggregateService } from './delete-ingress-aggregate/delete-ingress-aggregate.service';
import { DeleteSiteAggregateService } from './delete-site-aggregate/delete-site-aggregate.service';
import { KubectlService } from './kubectl/kubectl.service';
import { UpgradeSiteAggregateService } from './upgrade-site-aggregate/upgrade-site-aggregate.service';

export const KubeAggregates = [
  CreateSiteAggregateService,
  CreateIngressAggregateService,
  DeleteSiteAggregateService,
  DeleteIngressAggregateService,
  KubectlService,
  UpgradeSiteAggregateService,
];
