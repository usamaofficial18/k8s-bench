import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { KubectlService } from '../kubectl/kubectl.service';
import { DeleteIngressAggregateService } from './delete-ingress-aggregate.service';

describe('DeleteIngressAggregateService', () => {
  let service: DeleteIngressAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeleteIngressAggregateService,
        { provide: KubectlService, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<DeleteIngressAggregateService>(
      DeleteIngressAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
