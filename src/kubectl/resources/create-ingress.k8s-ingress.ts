import { NetworkingV1beta1Ingress } from '@kubernetes/client-node';
import { IngressConfig } from '../providers/ingress-config.provider';

/**
 * GenerateIngressTemplate
 * @param siteName FQDN of site
 * @param serviceName kubernetes service for ingress
 * @param ingressConfig annotations config
 * @param wildCardDomain '*.example.com'
 * @param wildCardTlsSecretName name of kubernetes secret for wildcard certificate
 */
export const generateIngressTemplate = (
  siteName: string,
  serviceName: string,
  ingressConfig: IngressConfig,
  wildCardDomain?: string,
  wildCardTlsSecretName?: string,
): NetworkingV1beta1Ingress => {
  const ingress = {
    metadata: {
      name: `${siteName}`,
      labels: ingressConfig.labels,
      annotations: ingressConfig.annotations,
    },
    spec: {
      rules: [
        {
          host: siteName,
          http: {
            paths: [
              {
                backend: {
                  serviceName,
                  servicePort: 80 as any,
                },
                path: '/',
              },
            ],
          },
        },
      ],
      tls: [],
    },
  };

  if (wildCardDomain && wildCardTlsSecretName) {
    ingress.spec.tls.push({
      hosts: [wildCardDomain],
      secretName: wildCardTlsSecretName,
    });
  } else {
    ingress.spec.tls.push({
      hosts: [siteName],
      secretName: `${siteName}-tls`,
    });
  }

  return ingress;
};
