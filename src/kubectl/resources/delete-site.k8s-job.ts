import { V1Job } from '@kubernetes/client-node';
import {
  ERPNEXT_WORKER_IMAGE,
  SITES_DIR,
  DELETE_SITE,
} from '../../constants/app-constants';

export const generateDeleteSiteJobJsonTemplate = (
  siteName: string,
  namespace: string,
  version: string,
  pvc: string,
  svcName: string,
  benchUid: string,
  mariadbRootPasswordSecretName: string,
  deleteSiteConfigMapName: string,
  imagePullSecretName?: string,
  rootUser: string = 'root',
): V1Job => {
  return {
    metadata: {
      name: `${DELETE_SITE}-${siteName}`,
      namespace,
      annotations: {
        benchService: svcName,
        benchUid,
      },
    },
    spec: {
      backoffLimit: 0,
      template: {
        spec: {
          imagePullSecrets: imagePullSecretName
            ? [{ name: imagePullSecretName }]
            : [],
          securityContext: {
            supplementalGroups: [1000],
          },
          containers: [
            {
              name: DELETE_SITE,
              image: `${ERPNEXT_WORKER_IMAGE}:${version}`,
              command: ['/home/frappe/frappe-bench/env/bin/python'],
              args: ['/opt/frappe/delete_site.py'],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
                {
                  name: DELETE_SITE,
                  mountPath: '/opt/frappe',
                },
              ],
              env: [
                { name: 'SITE_NAME', value: siteName },
                { name: 'DB_ROOT_USER', value: rootUser },
                {
                  name: 'DB_ROOT_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      key: 'password',
                      name: mariadbRootPasswordSecretName,
                    },
                  },
                },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
            {
              name: DELETE_SITE,
              configMap: {
                name: deleteSiteConfigMapName,
              },
            },
          ],
        },
      },
    },
  };
};
