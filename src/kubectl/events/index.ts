import { NewSiteCreatedHandler } from './new-site-created/new-site-created.handler';
import { CreateSiteJobFailedHandler } from './create-site-job-failed/create-site-job-failed.handler';
import { SiteIngressDeletedHandler } from './site-ingress-deleted/site-ingress-deleted.handler';
import { SiteUpgradedHandler } from './site-upgraded/site-upgraded.handler';

export const KubeEventHandlers = [
  NewSiteCreatedHandler,
  CreateSiteJobFailedHandler,
  SiteIngressDeletedHandler,
  SiteUpgradedHandler,
];
