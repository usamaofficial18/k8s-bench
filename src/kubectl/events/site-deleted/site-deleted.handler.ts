import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SiteDeletedEvent } from './site-deleted.event';

@EventsHandler(SiteDeletedEvent)
export class SiteIngressDeletedHandler implements IEventHandler {
  constructor() {}

  handle(event: SiteDeletedEvent) {}
}
