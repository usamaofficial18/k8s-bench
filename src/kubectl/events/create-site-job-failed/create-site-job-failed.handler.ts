import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { CreateSiteJobFailedEvent } from './create-site-job-failed.event';

@EventsHandler(CreateSiteJobFailedEvent)
export class CreateSiteJobFailedHandler implements IEventHandler {
  constructor() {}

  handle(event: CreateSiteJobFailedEvent) {
    // Todo Fire Webhook
    event;
  }
}
