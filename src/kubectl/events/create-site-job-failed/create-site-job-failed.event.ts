import { IEvent } from '@nestjs/cqrs';
import { V1Job } from '@kubernetes/client-node';

export class CreateSiteJobFailedEvent implements IEvent {
  constructor(public readonly siteName: string, public readonly job?: V1Job) {}
}
