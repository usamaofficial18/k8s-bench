import { IEvent } from '@nestjs/cqrs';
import { V1Job } from '@kubernetes/client-node';

export class SiteUpgradedEvent implements IEvent {
  constructor(public readonly namespace: string, public readonly job: V1Job) {}
}
