import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NewSiteCreatedEvent } from './new-site-created.event';

@EventsHandler(NewSiteCreatedEvent)
export class NewSiteCreatedHandler implements IEventHandler {
  constructor() {}

  handle(event: NewSiteCreatedEvent) {}
}
