import { IEvent } from '@nestjs/cqrs';

export class SiteIngressDeletedEvent implements IEvent {
  constructor(public readonly ingressName: string) {}
}
