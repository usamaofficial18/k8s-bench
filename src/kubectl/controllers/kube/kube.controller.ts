import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateNewSiteJobDto } from '../dtos/create-new-site-job.dto';
import { CreateNewSiteJobCommand } from '../../commands/create-new-site-job/create-new-site-job.command';
import { DeleteSiteJobDto } from '../dtos/delete-site-job.dto';
import { DeleteSiteCommand } from '../../commands/delete-site/delete-site.command';
import { BenchGuard } from '../../../auth/guards/bench/bench.guard';
import { CreateSiteIngressCommand } from '../../commands/create-site-ingress/create-site-ingress.command';
import { DeleteSiteResourcesCommand } from '../../commands/delete-site-resources/delete-site-resources.command';
import { CreateSiteIngressDto } from '../dtos/create-site-ingress.dto';
import { GetJobStatusQuery } from '../../queries/get-job-status/get-job-status.query';
import { DeleteSiteResourcesDto } from '../dtos/delete-site-resources.dto';
import { UpgradeSiteCommand } from '../../commands/upgrade-site/upgrade-site.command';
import { UpgradeSiteDto } from '../dtos/upgrade-site-job.dto';
import { PatchIngressDto } from '../dtos/patch-ingress.dto';
import { PatchIngressCommand } from '../../commands/patch-ingress/patch-ingress.command';
import { ApiBasicAuth } from '@nestjs/swagger';

@Controller('kube')
export class KubeController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/new_site_request')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async createNewSiteJobEvent(
    @Body() payload: CreateNewSiteJobDto,
    @Req() req,
  ) {
    return await this.commandBus.execute(
      new CreateNewSiteJobCommand(payload, req),
    );
  }

  @Post('v1/delete_site_request')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async deleteSiteJobEvent(@Body() payload: DeleteSiteJobDto, @Req() req) {
    return await this.commandBus.execute(new DeleteSiteCommand(payload, req));
  }

  @Post('v1/create_site_ingress')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async createSiteIngress(@Body() payload: CreateSiteIngressDto) {
    return await this.commandBus.execute(
      new CreateSiteIngressCommand(
        payload.svcName,
        payload.jobName,
        payload.namespace,
        payload.wildcardDomain,
        payload.wildcardTlsSecretName,
      ),
    );
  }

  @Post('v1/delete_site_resources')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async deleteSiteResources(@Body() payload: DeleteSiteResourcesDto) {
    return await this.commandBus.execute(
      new DeleteSiteResourcesCommand(payload.siteName),
    );
  }

  @Get('v1/get_job_status/:jobName')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  async getJob(@Param('jobName') jobName: string) {
    return await this.queryBus.execute(new GetJobStatusQuery(jobName));
  }

  @Post('v1/upgrade_site')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async upgradeSiteResources(@Body() payload: UpgradeSiteDto) {
    return await this.commandBus.execute(new UpgradeSiteCommand(payload));
  }

  @Post('v1/patch_ingress')
  @UseGuards(BenchGuard)
  @ApiBasicAuth()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async upatchIngress(@Body() payload: PatchIngressDto) {
    return await this.commandBus.execute(new PatchIngressCommand(payload));
  }
}
