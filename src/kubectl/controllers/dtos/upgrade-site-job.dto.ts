import { ApiProperty } from '@nestjs/swagger';
import { IsFQDN, IsString } from 'class-validator';

export class UpgradeSiteDto {
  @IsFQDN()
  @ApiProperty()
  siteName: string;

  @IsString()
  @ApiProperty()
  basePyImage: string;

  @IsString()
  @ApiProperty()
  basePvc: string;
}
