import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TerminusModule } from '@nestjs/terminus';
import { KubeAggregates } from './aggregates';
import { KubeCommandHandlers } from './commands';
import { KubeController } from './controllers/kube/kube.controller';
import { KubeEventHandlers } from './events';
import { IngressConfigProvider } from './providers/ingress-config.provider';
import { KubeQueryHandlers } from './queries';

@Module({
  imports: [CqrsModule, TerminusModule, HttpModule],
  controllers: [KubeController],
  providers: [
    ...KubeEventHandlers,
    ...KubeCommandHandlers,
    ...KubeAggregates,
    ...KubeQueryHandlers,
    IngressConfigProvider,
  ],
})
export class KubectlModule {}
