### Install K3d Cluster

```shell
k3d cluster create devcluster \
  --api-port 127.0.0.1:6443 \
  -p 80:80@loadbalancer \
  -p 443:443@loadbalancer \
  --k3s-server-arg "--no-deploy=traefik"
```

### Add Helm Repo

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add frappe https://helm.erpnext.com
helm repo update
```

### Install MariaDB

```shell
kubectl create ns mariadb
helm install mariadb -n mariadb bitnami/mariadb -f ./docs/example/mariadb-values.yaml
```

### Install NFS

```shell
kubectl create ns nfs
kubectl create -f ./docs/example/nfs-server-provisioner/statefulset.dev.yaml
kubectl create -f ./docs/example/nfs-server-provisioner/rbac.yaml
kubectl create -f ./docs/example/nfs-server-provisioner/class.yaml
```

### Install Redis

```shell
kubectl create ns redis
helm install redis -n redis bitnami/redis \
  --set auth.enabled=false \
  --set auth.sentinal=false \
  --set architecture=standalone \
  --set master.persistence.enabled=false
```

### Install ERPNext

```shell
kubectl create ns erpnext
cp ./docs/example/erpnext-example-values.yaml ./docs/example/erpnext-values.yaml
editor ./docs/example/erpnext-values.yaml
helm install erpnext-dev -n erpnext frappe/erpnext -f ./docs/example/erpnext-values.yaml
```

Note: Create `imagePullSecrets` if necessary and set it in `values.yaml`

### Create resources

```
helm template dev -n erpnext deploy/k8s-bench --set isProdMode=false | kubectl -n erpnext apply -f -
```

### Install Ingress Controller (Optional)

Follow this step to try out created sites locally on port 80 and 443.

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.46.0/deploy/static/provider/cloud/deploy.yaml
```
